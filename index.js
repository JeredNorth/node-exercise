const fs           = require("fs/promises");
const express      = require("express");
const cors         = require("cors");
const _            = require("lodash");
const axios        = require("axios");

const app = express();

app.get("/people", async (req, res) => {
    let nextPage = 'https://swapi.dev/api/people/';

    let people = [];

    while (nextPage) {
      let nextres = await axios(nextPage)
      const { next, results } = await nextres.data;
      nextPage = next
      people = [...people, ...results]
    } 

    return res.json(_.sortBy(people, req.query.sortby));

});

app.get("/planets", async (req, res) => {
    const response = await axios.get('https://swapi.dev/api/planets')
    let planets = response.data.results;

    for(let i = 0; i < planets.length; i++) {

        let residentsArray = [];
        
        for (let element of planets[i].residents) {
            element = await getResident(element);
            residentsArray.push(element);

            planets[i].residents = residentsArray;
        }
    }

    return res.json(planets);
});


async function getResident(path) {
    resident = await axios.get(path);
    residentName = resident.data['name'];

    return residentName;
}


app.listen(3000, () => console.log("API server is running..."));
